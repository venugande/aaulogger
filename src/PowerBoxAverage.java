import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class PowerBoxAverage {
	public static void main(String[] args) {
		try {
			DecimalFormat f = new DecimalFormat("##.#");
			f.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
			BufferedReader inFile = new BufferedReader(new FileReader(
					FileParser.getFilePathPrefix() + "powerbox.log"));
			BufferedWriter outFile = new BufferedWriter(new FileWriter(
					FileParser.getFilePathPrefix() + "powerbox_averaged.log"));
			String str = inFile.readLine(), tempOut="",pre_time="";
			outFile.write("time,power_avg \n");
			int count = 0 ;//,skip=0,iter=0;
			double total = 0;
			String[] temp;
			double time_interval=1;
			double current_sec=time_interval,time;
			while ((str = inFile.readLine()) != null) {
				
				temp = str.split(",");
				time=Double.parseDouble(temp[0]);
				count+=1;
				
				//if (count == num_values_to_average) {
				if(time>=current_sec){
					//System.out.println(current_sec);
					tempOut = tempOut+ pre_time+","+(total / count) + "\n";
					System.out.println("time:"+pre_time +" ,absPower:"+Double.parseDouble(temp[1])+" ,power:"+(total/count)+" ,total:"+total+" ,count:"+count);
					count = 0;
					total = 0;
				//	System.out.println(current_sec+time_interval);
					current_sec=Double.parseDouble(f.format(current_sec+time_interval));
					if (tempOut.length() > 2048) {
						outFile.write(tempOut);
						outFile.flush();
						tempOut = "";
					}

				}
				pre_time=temp[0];
				total += Double.parseDouble(temp[1]);

			}
			outFile.write(tempOut);
			outFile.flush();
			inFile.close();
			outFile.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
