import java.net.*;
import java.io.*;

public class SimpleServer {
	public static void main(String args[]) throws IOException {
		// Register service on port 4444
		ServerSocket s = new ServerSocket(4444);
		System.out.println("Listening on port 4444");
		while(true)
		{
		final Socket s1 = s.accept();// Wait and accept a connection
		System.out.println("Incoming connection accpeted:"
				+ s1.getRemoteSocketAddress());

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// Get a communication stream associated with the socket
				try {
					OutputStream s1out = s1.getOutputStream();
					DataOutputStream dos = new DataOutputStream(s1out);
					// Send a string!

					dos.writeUTF(System.currentTimeMillis() + "");

					// Close the connection, but not the server socket
					dos.close();
					s1out.close();
					s1.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

	}
	}
}