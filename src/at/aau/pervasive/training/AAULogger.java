package at.aau.pervasive.training;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import at.aau.pervasive.components.GPS;

public class AAULogger extends Activity implements OnClickListener {
	private final static String TAG = AAULogger.class.getSimpleName();
	final static String BLUETOOTH = "bluetooth";
	final static String LCD = "lcd";

	final static String TIME_STAMPS = "time_stamps";
	// Layout views
	private RelativeLayout mRLayout;
	private Button mStartBluetoothButton;
	private Button mMakeDiscoverableButton;
	private Button mStartLCDButton;
	private Button mServiceStartutton;
	private Button mStartCPUButton;
	private Button mMasterButton;
	private Button mSlaveButton;
	private Button mStartGPSButton;
	private Button mStartTCPServerButoon;
	private EditText mNumOfSecET;

	private static final int MAKE_BLUETOOTH_DISCOVERABLE_CODE = 1;
	private final int OLED_SLEEP_INTERVAL = 5000;
	private File mOutFile;
	private BufferedOutputStream logOut;
	private static int color;
	private static int count;
	private static int colorValue;

	private Intent serviceIntent;
	private ITrainingService trainingService;
	private TrainingServiceConnection conn;
	private  BroadcastReceiver mReceiver;
	
	// connection type codes
	public static final int MASTER_MODE = 1;
	public static final int SLAVE_MODE = 2;

	//Member Thread to ping all connected devices
	private TrackingThread mTrackingThread;
	private OLEDTraining mOLEDTrainingThread;
	private boolean gpsOn=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		serviceIntent = new Intent(this, AAULoggerService.class);
		conn = new TrainingServiceConnection();

		setContentView(R.layout.main);
		mRLayout = (RelativeLayout) findViewById(R.id.rLayout);
		mStartBluetoothButton = (Button) findViewById(R.id.button_BT);
		mStartLCDButton = (Button) findViewById(R.id.button_LCD);
		mStartCPUButton = (Button) findViewById(R.id.button_CPU);
		mMakeDiscoverableButton = (Button) findViewById(R.id.makeDiscoverable_Button);
		mNumOfSecET = (EditText) findViewById(R.id.ET_BT_Time);
		mServiceStartutton = (Button) findViewById(R.id.startService_button);
        mMasterButton = (Button)findViewById(R.id.startBTServer);
        mSlaveButton = (Button)findViewById(R.id.startBTClient);
		mStartGPSButton = (Button) findViewById(R.id.startGPSTraining_Button);
        mStartTCPServerButoon = (Button)findViewById(R.id.startTCPServer_Button);
		
		mStartBluetoothButton.setOnClickListener(this);
		mStartLCDButton.setOnClickListener(this);
		mStartCPUButton.setOnClickListener(this);
		mMakeDiscoverableButton.setOnClickListener(this);
		mServiceStartutton.setOnClickListener(this);
		mMasterButton.setOnClickListener(this);
		mSlaveButton.setOnClickListener(this);
		mStartGPSButton.setOnClickListener(this);
		mStartTCPServerButoon.setOnClickListener(this);
		
		 mStartBluetoothButton.setEnabled(false);
         mStartLCDButton.setEnabled(false);
         mStartCPUButton.setEnabled(false);
         mMakeDiscoverableButton.setEnabled(false);
		 mMasterButton.setEnabled(false);
		 mSlaveButton.setEnabled(false);
         mStartGPSButton.setEnabled(false);
		 mStartTCPServerButoon.setEnabled(false);
         
		Log.i(TAG, "AAULoggerService Running :"+isLoggerServiceRunning());
		
	    mReceiver = new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(BluetoothTraining.ACTION_BLUETOOTH_DONE))
					mStartBluetoothButton.setEnabled(true);
				
			}
			
		};
		
		IntentFilter filter = new IntentFilter(); 
		filter.addAction(BluetoothTraining.ACTION_BLUETOOTH_DONE);
		
		registerReceiver(mReceiver, filter);
		

	}
	
	public static NetworkInterface getWifiNetworkInterface(WifiManager manager) {
		 
	    Enumeration<NetworkInterface> interfaces = null;
	    try {
	        //the WiFi network interface will be one of these.
	        interfaces = NetworkInterface.getNetworkInterfaces();
	    } catch (SocketException e) {
	        return null;
	    }
	     
	    //We'll use the WiFiManager's ConnectionInfo IP address and compare it with
	    //the ips of the enumerated NetworkInterfaces to find the WiFi NetworkInterface.
	 
	    //Wifi manager gets a ConnectionInfo object that has the ipAdress as an int
	    //It's endianness could be different as the one on java.net.InetAddress
	    //maybe this varies from device to device, the android API has no documentation on this method.
	    int wifiIP = manager.getConnectionInfo().getIpAddress();
	     
	    //so I keep the same IP number with the reverse endianness
	    int reverseWifiIP = Integer.reverseBytes(wifiIP);       
	 
	    while (interfaces.hasMoreElements()) {
	 
	        NetworkInterface iface = interfaces.nextElement();
	 
	        //since each interface could have many InetAddresses...
	        Enumeration<InetAddress> inetAddresses = iface.getInetAddresses();
	        while (inetAddresses.hasMoreElements()) {
	            InetAddress nextElement = inetAddresses.nextElement();
	            int byteArrayToInt = byteArrayToInt(nextElement.getAddress(),0);
	             
	            //grab that IP in byte[] form and convert it to int, then compare it
	            //to the IP given by the WifiManager's ConnectionInfo. We compare
	            //in both endianness to make sure we get it.
	            if (byteArrayToInt == wifiIP || byteArrayToInt == reverseWifiIP) {
	                return iface;
	            }
	        }
	    }
	 
	    return null;
	}
	 
	public static final int byteArrayToInt(byte[] arr, int offset) {
	    if (arr == null || arr.length - offset < 4)
	        return -1;
	 
	    int r0 = (arr[offset] & 0xFF) << 24;
	    int r1 = (arr[offset + 1] & 0xFF) << 16;
	    int r2 = (arr[offset + 2] & 0xFF) << 8;
	    int r3 = arr[offset + 3] & 0xFF;
	    return r0 + r1 + r2 + r3;
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		getApplicationContext().bindService(serviceIntent, conn, 0);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
	}

	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		getApplicationContext().unbindService(conn);
		unregisterReceiver(mReceiver);
		if(mTrackingThread!=null)
			mTrackingThread.interrupt();
		
		if(mOLEDTrainingThread !=null)
			mOLEDTrainingThread.interrupt();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.aaulogger, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_BT:

			try {
				if (trainingService.startBluetoothTraining())
					mStartBluetoothButton.setEnabled(false);
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			break;

		case R.id.startBTServer:
			   try {
				if(trainingService.startBTServer())
				   {
					    mMasterButton.setText("Running..");
						mSlaveButton.setVisibility(View.INVISIBLE);
						mMasterButton.setEnabled(false);
						pingConnectedDevices();
				   }
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			break;
			
		case R.id.startTCPServer_Button:
			try {
				if(trainingService.startTCPServer())
				{
					mStartTCPServerButoon.setText("Running..");
					 mStartTCPServerButoon.setEnabled(false);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			break;
			
		case R.id.startBTClient:
			try {
				if(trainingService.startBTClient())
		        	   mSlaveButton.setEnabled(false);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			break;

		case R.id.button_CPU: 
			try {
				if(trainingService.startCPUTraining())
					mStartCPUButton.setEnabled(false);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			break;
			
		case R.id.startGPSTraining_Button:
			
//			try {
//				if(trainingService.startGPSTraining())
//					mStartGPSButton.setEnabled(false);
//			} catch (RemoteException e) {
//				e.printStackTrace();
//			}
			
			if(!gpsOn)
				GPS.getInstance(getApplicationContext()).requestForUpdates(20000);
			else
				GPS.getInstance(getApplicationContext()).removeUpdates();
			
			gpsOn=!gpsOn;
			
			
			break;
			
		case R.id.button_LCD:
			mStartBluetoothButton.setVisibility(View.INVISIBLE);
			mMakeDiscoverableButton.setVisibility(View.INVISIBLE);
			mStartLCDButton.setVisibility(View.INVISIBLE);
			//mServiceStartutton.setVisibility(View.INVISIBLE);
			mNumOfSecET.setVisibility(View.INVISIBLE);
			mSlaveButton.setVisibility(View.INVISIBLE);
			mStartCPUButton.setVisibility(View.INVISIBLE);
			mStartGPSButton.setVisibility(View.INVISIBLE);
			mStartTCPServerButoon.setVisibility(View.INVISIBLE);
			mMasterButton.setVisibility(View.INVISIBLE);
			color = Color.BLACK;
			if(mOLEDTrainingThread==null)
			{
				mOLEDTrainingThread = new OLEDTraining();
                mOLEDTrainingThread.start();
			}
			 break;

		case R.id.startService_button:
			mServiceStartutton.setEnabled(false);
			if (trainingService != null)
				stopService(serviceIntent);
			else
				new TimeSynctask().execute("");
			break;

		case R.id.makeDiscoverable_Button:
			int time = Integer
					.parseInt(mNumOfSecET.getText().toString().trim());
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, time);
			startActivityForResult(discoverableIntent,
					MAKE_BLUETOOTH_DISCOVERABLE_CODE);
			break;
		}

	}

	private void pingConnectedDevices()
	{
		mTrackingThread=new TrackingThread();
		mTrackingThread.start();
	}
	
	class TimeSynctask extends AsyncTask<String, Integer, Long[]> {

		@Override
		protected Long[] doInBackground(String... params) {
			long start_time, end_time = 0;
			start_time = SystemClock.elapsedRealtime();
			String input;
			long serverTime=0;
//			try {
//				
//				// Open your connection to a server, at port 4444
//				 Socket s1 = new Socket("144.205.142.88",4444);	
//				 Log.d(TAG, "Connection established with :" +s1.getRemoteSocketAddress());
//				 // Get an input file handle from the socket and read the input
//				 InputStream s1In = s1.getInputStream();
//				 DataInputStream dis = new DataInputStream(s1In);
//				 String st = new String (dis.readUTF());
//				 serverTime = Long.parseLong(st);
//					end_time= SystemClock.elapsedRealtime();				
//					serverTime+= (end_time-start_time)/2;
//					//SystemClock.setCurrentTimeMillis(serverTime);					
//					Log.i(TAG, "ServerTime:"+serverTime);
//					Log.i(TAG, "LocalTime:"+System.currentTimeMillis());
//				 //System.out.println(st);
//				 // When done, just close the connection and exit
//				 dis.close();
//				 s1In.close();
//				 s1.close();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				Log.e(TAG,e.getMessage());
//				e.printStackTrace();
//			}
			
			return new Long[]{serverTime , end_time} ;
		}

		@Override
		protected void onPostExecute(Long[] result) {

			mServiceStartutton.setText(result[0] + "");
			serviceIntent.putExtra(TIME_STAMPS, new long[] { result[0],
					result[1] });
			startService(serviceIntent);

			serviceIntent.removeExtra(TIME_STAMPS);
			super.onPostExecute(result);
		}

	}

	private boolean isLoggerServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (AAULoggerService.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private class TrainingServiceConnection implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.i(TAG, "service Connected");
			trainingService = ITrainingService.Stub.asInterface(service);
			mServiceStartutton.setText("Stop");
			mServiceStartutton.setEnabled(true);
			mStartBluetoothButton.setEnabled(true);
			mStartLCDButton.setEnabled(true);
			mMakeDiscoverableButton.setEnabled(true);
			mStartCPUButton.setEnabled(true);
			mMasterButton.setEnabled(true);
			mSlaveButton.setEnabled(true);
			mStartGPSButton.setEnabled(true);
		    mStartTCPServerButoon.setEnabled(true);

		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.i(TAG, "service Disconnected");
			trainingService = null;
			getApplicationContext().unbindService(conn);
			getApplicationContext().bindService(serviceIntent, conn, 0);
			mServiceStartutton.setText("Start");
			mServiceStartutton.setEnabled(true);
			mStartBluetoothButton.setEnabled(false);
			mStartCPUButton.setEnabled(false);
			mStartLCDButton.setEnabled(false);
			mMakeDiscoverableButton.setEnabled(false);
			mMasterButton.setEnabled(false);
			mSlaveButton.setEnabled(false);
			mStartGPSButton.setEnabled(false);
		    mStartTCPServerButoon.setEnabled(false);

		}

	}

	private class OLEDTraining extends Thread {

		public void run() {

			while (count < 21 && !Thread.interrupted()) {

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mRLayout.setBackgroundColor(color);
					}
				});

				try {
					Thread.sleep(OLED_SLEEP_INTERVAL);
                   Log.d(TAG, "count:"+count);
					if (count % 5 == 0)
						colorValue = 0;
					colorValue += 50;
					if (count < 5)
						color = Color.rgb(colorValue, 0, 0);
					else if (count < 10)
						color = Color.rgb(0, colorValue, 0);
					else if (count < 15)
						color = Color.rgb(0, 0, colorValue);
					else
						color = Color.rgb(colorValue,colorValue,colorValue);
					count++;

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
//			runOnUiThread(new Runnable() {
//				
//				@Override
//				public void run() {
//					mStartBluetoothButton.setVisibility(View.VISIBLE);
//					mMakeDiscoverableButton.setVisibility(View.VISIBLE);
//					mStartLCDButton.setVisibility(View.VISIBLE);
//					mServiceStartutton.setVisibility(View.VISIBLE);
//					mNumOfSecET.setVisibility(View.VISIBLE);
//					mSlaveButton.setVisibility(View.VISIBLE);
//					mStartCPUButton.setVisibility(View.VISIBLE);
//					mStartGPSButton.setVisibility(View.VISIBLE);
//					mStartTCPServerButoon.setVisibility(View.VISIBLE);
//					mMasterButton.setVisibility(View.VISIBLE);
//				}
//			});
//			
		}

	}
	
	 private class TrackingThread extends Thread
	    {
	    	BlutoothTracker.ConnectedThread c;
	    	public void run()
	    	{
	    		while (!Thread.interrupted()) {
					try {
						for (BlutoothTracker.ConnectedThread t : BlutoothTracker.mConnected_slaves) {
							c=t;
							t.write(new String((Math.random()*100)+"").getBytes());
							
						}
						
						
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (IOException e) {
						BlutoothTracker.mConnected_slaves.remove(c);
					}
				}
	    		
	    		BlutoothTracker.mConnected_slaves.removeAllElements();
	    		
	    	}
	    }
	 
	 class LocationAsync extends AsyncTask<String, Integer, String>
	 {

		@Override
		protected void onPreExecute() {
			GPS.getInstance(getApplicationContext()).requestForUpdates(10);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			return null;
		}
		 
		@Override
		protected void onPostExecute(String result) {
			GPS.getInstance(getApplicationContext()).removeUpdates();
			super.onPostExecute(result);
		}

	 }

}
