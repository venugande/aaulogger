package at.aau.pervasive.training;

interface ITrainingService{

boolean startBluetoothTraining();
boolean startLCDTraining();
boolean startCPUTraining();
boolean startBTServer();
boolean startBTClient();
boolean startGPSTraining();
boolean startTCPServer();
}