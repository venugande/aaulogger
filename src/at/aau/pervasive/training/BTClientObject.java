package at.aau.pervasive.training;

import java.io.IOException;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BTClientObject implements Runnable {

	private String mSocketType;
	private final BluetoothSocket mSocket;
	private final static String TAG = BTClientObject.class.getSimpleName();
	private static final UUID RECEIVER_UUID = UUID
			.fromString("c1a24e26-10f0-11e3-ab8c-f23c91aec05e"); 
	private ConnectedObject mConnectedObject;
	private Thread mConnectedThread;

	public BTClientObject(BluetoothDevice device, boolean secure) {
		BluetoothSocket tmp = null;
		mSocketType = secure ? "Secure" : "Insecure";
		try {
			if (secure)
				tmp = device.createRfcommSocketToServiceRecord(RECEIVER_UUID);
			else
				tmp = device.createInsecureRfcommSocketToServiceRecord(RECEIVER_UUID); 
			
			Log.i(TAG, "BTClient Object created:"+tmp);
		} catch (IOException e) {
			Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			e.printStackTrace();
		}
		mSocket = tmp;
	
	}

	public void run() {
		// Make a connection to the BluetoothSocket
		try {
			// This is a blocking call and will only return on a
			// successful connection or an exception
			if(mConnectedThread==null)
			mSocket.connect();
		} catch (IOException e) {
			Log.e(TAG,"IOEXception while trying to connect");
			e.printStackTrace();
			try {
				mSocket.close();
			} catch (IOException e2) {
				Log.e(TAG, "unable to close() " + mSocketType
						+ " socket during connection failure", e2);
			}
			return;

		}

		if (mConnectedThread != null) {
			mConnectedThread.interrupt();
			while (mConnectedThread.isAlive()) {
				try {
					mConnectedThread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			mConnectedThread=null;
		}
		
		if(mConnectedObject==null)
			mConnectedObject= new ConnectedObject(mSocket, 0);
		
		// Start the connected thread
		mConnectedThread = new Thread(mConnectedObject);
		mConnectedThread.start();
	}
	
	public void killConnection()
	{
		if(mConnectedObject!=null)
		mConnectedObject.closeStreams();
	}

}
