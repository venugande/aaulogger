package at.aau.pervasive.training;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.util.Log;

public class ConnectedObject implements Runnable {

	private final static String TAG = ConnectedObject.class.getSimpleName();
	private BluetoothSocket mmClientSocket;
	private final BufferedInputStream fin;
	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private File mFile =new File(Environment.getExternalStorageDirectory(), "img.jpeg");
	private int mConnectionMode; // 0 Send 1 Receive

	public ConnectedObject(BluetoothSocket socket, int connectionMode) {
		mmClientSocket = socket;
		mConnectionMode = connectionMode;
		InputStream tmpIn = null;
		OutputStream tmpOut = null;
		BufferedInputStream tmpFIn=null;
		// Get the input and output streams, using temp objects because
		// member streams are final
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
			tmpFIn = new BufferedInputStream(new FileInputStream(mFile),1024);
		} catch (IOException e) {
		}

		mmInStream = tmpIn;
		mmOutStream = tmpOut;
		fin=tmpFIn;
	}

	public void run() {
		byte[] buffer = new byte[1024];
		int num_bytes;
		int packetsize=1024;
		double nosofpackets=Math.ceil(((int) mFile.length())/packetsize);
		Log.i(TAG, "num_of_packets to send:"+nosofpackets);
		if (mConnectionMode == 1) {
			while (!Thread.interrupted()) {
				try {
					num_bytes = mmInStream.read(buffer);
					Log.i(TAG, "received:"+num_bytes+"bytes:" + new String(buffer));
				} catch (IOException e) {
					break;
				}

			}
			
			
		}
		else
		{
			
			try {
				for(int i=0;i<nosofpackets+1;i++)
				{
					fin.read(buffer);
					Log.i(TAG, "Packet:"+(i+1));
				    mmOutStream.write(buffer);
				    mmOutStream.flush();
				    ComponentsState.bt_packets_sent++;
				}
				Log.i(TAG, "file sent..");
				
			} catch (IOException e) {
				
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
			
		}

	}
	
	public void closeStreams() {
		try {
			mmOutStream.close();
			fin.close();
			mmClientSocket.close();
			Log.d(TAG, "N700 client socket closed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
