package at.aau.pervasive.training;

import java.util.Set;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class AAULoggerService extends Service {

	private static final String TAG = AAULoggerService.class.getSimpleName();
	private BluetoothTraining mBluetoothTraining;
	private ComponentsState mCompsState;
	private CPUTraining mCPUTraining;
	private GPSTraining mGPSTraining;

	private Thread mComponentStateLogger;
	private Thread mBluetoothTrainingThread;
	private Thread mCPUTrainingThread;
	// private LCDTraining mLCDTrainingThread;
	private Thread mGPSTrainingThread;

	private BlutoothTracker mTrackerService;
	private TCPServer mTCPServer;

	@Override
	public void onCreate() {
		Log.i(TAG, "onCreate()");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "AAULoggerService started..");
		mCompsState = new ComponentsState(this,
				intent.getLongArrayExtra(AAULogger.TIME_STAMPS));
		mComponentStateLogger = new Thread(mCompsState);
		mComponentStateLogger.start();

		mBluetoothTraining = new BluetoothTraining(this);
		mCPUTraining = new CPUTraining(this);

		mTrackerService = new BlutoothTracker();
		mTCPServer = new TCPServer(this);

		mGPSTraining = new GPSTraining(this);
		Log.i(TAG, "onStratCommand()");

		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {

		Log.i(TAG, "onDestroy()");
		if (mComponentStateLogger != null) {
			mComponentStateLogger.interrupt();
			while (mComponentStateLogger.isAlive()) {
				try {
					mComponentStateLogger.join();
				} catch (InterruptedException e) {
				}
			}
		}

		if (mBluetoothTrainingThread != null) {
			mBluetoothTraining.onExit();
			mBluetoothTrainingThread.interrupt();
			while (mBluetoothTrainingThread.isAlive()) {
				try {
					mBluetoothTrainingThread.join();
				} catch (InterruptedException e) {
				}
			}
		}

		if (mCPUTrainingThread != null) {
			mCPUTrainingThread.interrupt();
			while (mCPUTrainingThread.isAlive()) {
				try {
					mCPUTrainingThread.join();
				} catch (InterruptedException e) {
				}
			}
			mCPUTraining.onExit();
		}

		if (mTrackerService != null)
			mTrackerService.stop();

		if(mTCPServer != null)
			mTCPServer.stop();
		
		Log.d(TAG, "AAULoggerService stoped..");
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "onBind()");
		return binder;
	}

	private final ITrainingService.Stub binder = new ITrainingService.Stub() {

		@Override
		public boolean startLCDTraining() throws RemoteException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean startBluetoothTraining() throws RemoteException {
			// TODO Auto-generated method stub
			if (mBluetoothTraining == null)
				return false;
			if (mBluetoothTrainingThread != null)
				mBluetoothTrainingThread.interrupt();

			mBluetoothTrainingThread = new Thread(mBluetoothTraining);
			mBluetoothTrainingThread.start();
			return true;
		}

		@Override
		public boolean startCPUTraining() throws RemoteException {
			if (mCPUTraining == null)
				return false;

			if (mCPUTrainingThread != null)
				mCPUTrainingThread.interrupt();

			mCPUTrainingThread = new Thread(mCPUTraining);
			mCPUTrainingThread.start();
			return true;

		}

		@Override
		public boolean startBTServer() throws RemoteException {
			// TODO Auto-generated method stub
			if (mTrackerService != null) {
				mTrackerService.start();
				return true;
			}
			return false;
		}

		@Override
		public boolean startBTClient() throws RemoteException {
			Set<BluetoothDevice> pairedDevices = BluetoothAdapter
					.getDefaultAdapter().getBondedDevices();
			if (pairedDevices.size() > 0) {

				for (BluetoothDevice bluetoothDevice : pairedDevices) {
					Log.i(TAG,
							"trying to connect...to:"
									+ bluetoothDevice.getName());
					mTrackerService.connect(bluetoothDevice, false);
					break;
				}
				return true;
			}

			return false;
		}

		@Override
		public boolean startGPSTraining() throws RemoteException {

			if (mGPSTraining == null)
				return false;

			if (mGPSTrainingThread != null)
				mGPSTrainingThread.interrupt();

			mGPSTrainingThread = new Thread(mGPSTraining);
			mGPSTrainingThread.start();
			return true;
		}

		@Override
		public boolean startTCPServer() throws RemoteException {
			if (mTCPServer != null) {
				mTCPServer.start();
				return true;
			}
			return false;
		}
	};
}
