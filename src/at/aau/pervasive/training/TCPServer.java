package at.aau.pervasive.training;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

public class TCPServer {

	public static final String TAG = TCPServer.class.getSimpleName();
	private AcceptThread mAcceptThread;
	private ConnectedThread mConnectedThread;

	private UDPServer mUDPServer;
    private TCPClient mTCPClient;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;
    
    public TCPServer(Context context) {
    	mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "aauWakeLock");	
		}
    
	public void start() {
		if (mAcceptThread != null) {
			mAcceptThread.interrupt();
			mAcceptThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread.interrupt();
			mConnectedThread = null;
		}
		
		if(mTCPClient != null)
		{
			mTCPClient.interrupt();
			mTCPClient = null;
		}

		// if (mAcceptThread == null) {
		// mAcceptThread = new AcceptThread();
		// mAcceptThread.start();
		// }

		// if (mUDPServer == null) {
		// mUDPServer = new UDPServer();
		// mUDPServer.start();
		// }

		if(mTCPClient == null)
		{
			mTCPClient = new TCPClient();
			mTCPClient.start();
		}
	}

	public void stop() {
		if (mAcceptThread != null) {
			mAcceptThread.interrupt();
			mAcceptThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread.interrupt();
			mConnectedThread = null;
		}

		if (mUDPServer != null) {
			mUDPServer.interrupt();
			mUDPServer = null;
		}
		
		if(mTCPClient != null)
		{
			mTCPClient.interrupt();
			mTCPClient = null;
		}
	}

	private class UDPServer extends Thread {

		public void run() {
			try {
				DatagramSocket serverSocket = new DatagramSocket(7777);
				byte[] receiveData = new byte[1024];

				while (!Thread.interrupted()) {
					DatagramPacket receivePacket = new DatagramPacket(
							receiveData, receiveData.length);
					serverSocket.receive(receivePacket);
					String sentence = new String(receivePacket.getData());
					// Log.i(TAG,"RECEIVED: " + sentence);
				}
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class TCPClient extends Thread {

		public void run() {
			Socket s;
			try {
				s = new Socket("54.213.149.143", 50000);
				File file = new File("/mnt/sdcard/Gundello.mp3");
				BufferedInputStream fin = new BufferedInputStream(
						new FileInputStream(file));
				OutputStream out = s.getOutputStream();
				byte[] buffer = new byte[1024];
				int numOfPackets = (int) Math.ceil(((int) file.length())
						/ buffer.length);
				// boolean fileSent=false;
				long sleepTime = 500;
				int n = 100, c = 0, d = 100;

				mWakeLock.acquire();
				for (int i = 1; i < 1000 + 1; i++) {
					if (i % n == 0) {
						sleepTime = 20000;
//						c += 4;
						n+= 100;
//
//						sleepTime = 1000 / c;
//						if (sleepTime < 100)
//							sleepTime = 0;
						
					}else
						sleepTime=500;
					
					fin.read(buffer);
					out.write(buffer);
					out.flush();
					//Log.d(TAG,"sleepTime:" + sleepTime + ",n:" + n);
					Thread.sleep(sleepTime);

//					if (sleepTime < 250 && i % 10 == 0)
//						Thread.sleep(1000);
				}
				
				if(mWakeLock.isHeld())
					mWakeLock.release();
				
				fin.close();
				out.close();
				s.close();
				Log.d(TAG,"File sent");
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class AcceptThread extends Thread {
		private final ServerSocket mServerSocket;

		public AcceptThread() {
			ServerSocket tmp = null;
			try {
				tmp = new ServerSocket(7777);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mServerSocket = tmp;
		}

		public void run() {
			Log.d(TAG, "Accept Thread Running");
			while (true) {
				try {
					Socket clientSocket = mServerSocket.accept();
					mConnectedThread = new ConnectedThread(clientSocket);
					mConnectedThread.start();
					Log.d(TAG, "client Accepted");
					break;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try {
				mServerSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private class ConnectedThread extends Thread {
		private Socket mSocket;
		private final InputStream mInStream;

		public ConnectedThread(Socket clientSocket) {
			// TODO Auto-generated constructor stub
			mSocket = clientSocket;
			InputStream tmpIn = null;

			try {
				tmpIn = mSocket.getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mInStream = tmpIn;
		}

		public void run() {
			byte[] buffer = new byte[1600];
			while (!Thread.interrupted()) {
				try {
					mInStream.read(buffer);
					// Log.i(TAG, "read:" + new String(buffer));
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			try {
				mInStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void cancel() {
			try {
				mSocket.close();
				Log.d(TAG, "client Socket closed");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
