package at.aau.pervasive.training;

import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

public class CPUTraining implements Runnable {
	
	private final static String TAG = CPUTraining.class.getSimpleName();
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;
	public CPUTraining(Context context) {
		mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "aauWakeLock");
		
	}
	
	
	@Override
	public void run() {

		//int min=10000,max=100000;
		long start_time,end_time,loop_start_time;
		int max_interval=1000;
		int load_interval=max_interval/10;
		long remaining_time=0;
		NBodySystem bodies = new NBodySystem();
		loop_start_time=SystemClock.elapsedRealtime();
		mWakeLock.acquire();
		while(!Thread.interrupted()&&load_interval<max_interval)
		{
			
			try {
				//Thread.sleep(15000);
				
			
				//new nbody((int)(min+Math.random()*(max-min)));
				if(SystemClock.elapsedRealtime()>= loop_start_time+10000)
				{
					
					//load_interval+=100;
					
					load_interval=(int) (Math.random()*max_interval);
					
					//Log.i(TAG, load_interval+"");
					/*mWakeLock.release();
					Thread.sleep(15000);
					mWakeLock.acquire();*/
					loop_start_time=SystemClock.elapsedRealtime();
				}
				//Log.i(TAG, (SystemClock.elapsedRealtime()<(st1+interval))+"");
				start_time=SystemClock.elapsedRealtime();
				while(SystemClock.elapsedRealtime()<(start_time+load_interval))
				{
					bodies.advance(0.02);
				}
				end_time=SystemClock.elapsedRealtime();
				//Log.i(TAG, " timetookin_ms:"+(end_time-start_time));
				remaining_time=(max_interval-(end_time-start_time));
				Thread.sleep(remaining_time>0?remaining_time:0);
				
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
			
			
		}
		
		Log.d(TAG, "CPU Training Thread interrupted");
		if(mWakeLock.isHeld())
			mWakeLock.release();
	}


	public void onExit() {
		if(mWakeLock.isHeld())
			mWakeLock.release();
		
	}
	
}
