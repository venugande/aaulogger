package at.aau.pervasive.training;

import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

public class BluetoothTraining implements Runnable {

	private final static String TAG = BluetoothTraining.class.getSimpleName();

	private final String BLUETOOTH_RECEIVER_NAME = "bluetooth_receiver";
	private BluetoothAdapter mBluetoothAdapter;
	private final static int WAITING_INTERVAL = 30000;
	private static final int SLEEP_INTERVAL = 15000;
	private final Context mContext;
	private PowerManager mPowerManager;
	private PowerManager.WakeLock mWakeLock;
	public static final String ACTION_BLUETOOTH_DONE = "at.aau.pervasive.training.broadcast.action.BLUETOOTH_DONE";
	private BTClientObject mBTClientObject = null;
	private Thread mBTClientThread;

	public BluetoothTraining(Context context) {
		mContext = context;
		mPowerManager = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"aauWakeLock");
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		Log.i(TAG, "new Bluetooth Training Object Created");
	}

	public void run() {
		android.os.Process
				.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);


		// remote device discovery exercise
		for (int counter = 0; counter < 0 && !Thread.interrupted(); counter++) {
			mBluetoothAdapter.startDiscovery();
			// Log.e(TAG, "Discovergy started");

			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				Log.e(TAG, "Bluetooth Thread Interrupted!!");
				break;
			}
			if (mBluetoothAdapter.isDiscovering())
				mBluetoothAdapter.cancelDiscovery();
			// Log.e(TAG, "discovergy finished");
			try {
				Thread.sleep(SLEEP_INTERVAL);
			} catch (InterruptedException e) {
				Log.e(TAG, "Bluetooth Thread Interrupted!!");
				break;
			}

		}
		Log.d(TAG, "Bluetooth Thread finished running");

		mBluetoothAdapter.cancelDiscovery();

		// if(mBluetoothAdapter.isEnabled())
		// mBluetoothAdapter.disable();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Intent intent = new Intent(BluetoothTraining.ACTION_BLUETOOTH_DONE);
		mContext.sendBroadcast(intent,
				at.aau.pervasive.training.Manifest.permission.ALLOW);
		if (mWakeLock.isHeld())
			mWakeLock.release();
	}

	public void sendData() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
				.getBondedDevices();
		// If there are paired devices
		if (pairedDevices.size() > 0) {
			// Loop through paired devices
			for (BluetoothDevice device : pairedDevices) {
				Log.i(TAG, "trying to connect...to:" + device.getName());
				if (mBTClientThread != null) {
					mBTClientThread.interrupt();
					while (mBTClientThread.isAlive()) {
						try {
							mBTClientThread.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					mBTClientThread=null;
				}
				
				if(mBTClientObject!=null)
					mBTClientObject.killConnection();
			
				mBTClientObject=new BTClientObject(device, false);
				mBTClientThread = new Thread(mBTClientObject);
				mBTClientThread.start();
			}
		}

	}

	public void onExit() {
		if (mWakeLock.isHeld())
			mWakeLock.release();
	}


}
