package at.aau.pervasive.training;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.os.SystemClock;
import android.util.Log;

class TimeSyncThread implements Runnable
{
	
	
	
	@Override
	public void run()
	{
		long start_time,end_time;
		start_time= SystemClock.elapsedRealtime();
		
		try {
			
			Socket socket = new Socket("143.205.142.88" , 4444);
			DataInputStream in = new DataInputStream(socket.getInputStream());
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
			out.write("time");
			out.flush();
			long serverTime = in.readLong();
			end_time= SystemClock.elapsedRealtime();
			
			serverTime+= (end_time-start_time)/2;
			//SystemClock.setCurrentTimeMillis(serverTime);
			
			Log.i("TimeSync", "ServerTime:"+serverTime);
			
			out.write("done");
			out.flush();
			
			out.close();
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
}
