package at.aau.pervasive.training;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.TrafficStats;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;
import at.aau.pervasive.components.CPU;
import at.aau.pervasive.components.GPS;
import at.aau.pervasive.components.LCD;

public class ComponentsState implements Runnable {

	private final static String TAG = ComponentsState.class.getSimpleName();
	private final Context mContext;
	private final LCD mLCD;
	private final CPU mCPU;
	private final GPS mGPS;
	//private final WiFi mWiFi;
	private final WifiManager mWifiManager;
	private final File mOutFile;
	private BufferedOutputStream logOut; 
	private final static long MAX_INTERVAL = 1000;
	private final long[] time_stamps;
    private BroadcastReceiver broadcastReceiver;
    private int mBluetoothAdapter_State; 
    BluetoothAdapter mBluetoothAdapter;//=BluetoothAdapter.getDefaultAdapter();
    private long preUptime= SystemClock.uptimeMillis();
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;
    
    public static long bt_packets_sent=0,nt_packets_sent=0,nt_packets_received=0,nt_bytes_sent=0,nt_bytes_received=0;
    private long bt_preCount=-1,nt_ps_precount=-1,nt_pr_precount=-1,nt_bs_precount=-1,nt_br_precount=-1;
    private TrafficStats ts;
	public ComponentsState(Context context , long[] time_stamps) {
		

		
		mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "aauWakeLock");
		
		this.time_stamps = time_stamps;
		mContext =context;
		ts= new TrafficStats();
		mLCD = new LCD(context);
		mCPU = new CPU();
		mGPS =  GPS.getInstance(context);
		//mWiFi = new WiFi(context);
		mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		//mOutFile = new File(Environment.getExternalStorageDirectory(),
		//		"ComponentState" + System.currentTimeMillis() + ".log");
		String logFilename = context.getFileStreamPath(
                "ComponentState.log").getAbsolutePath();
		mOutFile = new File(logFilename);
		
		try {
			logOut = new BufferedOutputStream(new FileOutputStream(mOutFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//mWakeLock.acquire();
		
		mBluetoothAdapter_State =10;
		
		broadcastReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				//Log.e(TAG,intent.toUri(Intent.URI_INTENT_SCHEME));
				synchronized (this) {

					if (intent.getAction().equals(
							BluetoothAdapter.ACTION_STATE_CHANGED)) {

						switch (intent.getIntExtra(
								BluetoothAdapter.EXTRA_STATE, -1)) {
						case BluetoothAdapter.STATE_OFF:
							mBluetoothAdapter_State = 10;
							break;
							
						case BluetoothAdapter.STATE_TURNING_ON:
							mBluetoothAdapter_State = 11;
							break;
							
						case BluetoothAdapter.STATE_ON:
							mBluetoothAdapter_State =12;
							break;

						case BluetoothAdapter.STATE_TURNING_OFF:
							mBluetoothAdapter_State =13;
							break;
							
						
						default:
							mBluetoothAdapter_State = -1;
							break;
						}
					}
					else if (intent.getAction().equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
						mBluetoothAdapter_State = 4;
					else if (intent.getAction().equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
						mBluetoothAdapter_State = 5;
					else if (intent.getAction().equals(BluetoothDevice.ACTION_FOUND))
						mBluetoothAdapter_State = 6;
					else if (intent.getAction().equals(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED))
					{
						switch(intent.getIntExtra(BluetoothAdapter.EXTRA_CONNECTION_STATE, -1))
						{
						case BluetoothAdapter.STATE_CONNECTED:
							mBluetoothAdapter_State= 2;
							break;
						
						case BluetoothAdapter.STATE_CONNECTING:
						    mBluetoothAdapter_State=1;
						    break;
						 
						case BluetoothAdapter.STATE_DISCONNECTING:
							mBluetoothAdapter_State=3;
							break;
							
						case BluetoothAdapter.STATE_DISCONNECTED:
							mBluetoothAdapter_State=0;
							break;
						
						}
					}
					
					//Log.e(TAG, intent.getAction() +": "+mBluetoothAdapter_State);
				}
			};
		};

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
		intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
		mContext.registerReceiver(broadcastReceiver, intentFilter);

		
	}
	
	long getTransmittedPackets()
	{
		long count=0,packets_sent=TrafficStats.getTotalTxPackets();
		if(nt_ps_precount!=-1)
			count=packets_sent-nt_ps_precount;
		nt_ps_precount = packets_sent;
		nt_packets_sent=count;
		return count;
	}
	
	long getReceivedPackets()
	{
		long count=0,packets_received=TrafficStats.getTotalRxPackets();
		//Log.i(TAG, "received_packets:"+packets_received);
		if(nt_pr_precount!=-1)
			count=packets_received-nt_pr_precount;
		
		nt_pr_precount = packets_received;
		nt_packets_received=count;
		return count;
	}
	
	long getTransmittedBytes()
	{
		long count=0,bytes_sent=TrafficStats.getTotalTxBytes();
		if(nt_bs_precount!=-1)
			count=bytes_sent-nt_bs_precount;
		nt_bs_precount = bytes_sent;
		nt_bytes_sent=count;
		return count;
	}
	
	
	long getReceivedBytes()
	{
		long count=0,bytes_received=TrafficStats.getTotalRxBytes();
		if(nt_br_precount!=-1)
			count=bytes_received-nt_br_precount;
		nt_br_precount = bytes_received;
		nt_bytes_received=count;
		return count;
	}
	
	long getTransmittedPacketSize() {
		if (nt_packets_sent != 0)
			return nt_bytes_sent / nt_packets_sent;

		return 0;
	}

	long getReceivedPacketSize() {
		if (nt_packets_received != 0)
			return nt_bytes_received / nt_packets_received;

		return 0;
	}

	String getReceivedDatainMB()
	{
		
		if(nt_bytes_received>1024*1024)
			return nt_bytes_received/(1024*1024.0)+" MB";
		else if(nt_bytes_received>1024)
			return nt_bytes_received/1024.0+" KB";
		else
			return nt_bytes_received+"Bytes";
	}
	
	String getSentDatainMB()
	{
		if(nt_bytes_sent>1024*1024)
			return nt_bytes_sent/(1024*1024.0)+" MB";
		else if(nt_bytes_sent>1024)
			return nt_bytes_sent/1024.0+" KB";
		else
			return nt_bytes_sent+"Bytes";
	}
	
    long getBTPacketsSent()
	{
		//Log.d(TAG, packets_sent+","+preCount);
		long count=0;
		if(bt_preCount!=-1)
			count=bt_packets_sent-bt_preCount;

	  bt_preCount=bt_packets_sent;
	  return count;
	}

	int getBTStatus()
	{
		
		 if(mBluetoothAdapter.isDiscovering())
			 return 2;
		 else if(mBluetoothAdapter.isEnabled())
			 return 1;
		
		 return 0;
	}
	public void run() {
		android.os.Process
				.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
		long startTime, endTime, loopStartTime;
		String logString = "";
		loopStartTime = SystemClock.elapsedRealtime();
		long offset = SystemClock.elapsedRealtime() - time_stamps[1];

//		logString += "Time SystemClock :" + loopStartTime + ":serverTime:"
//				+ time_stamps[0] + ", Offset:" + offset + ": System "
//				+ System.currentTimeMillis() + "\n";
//		logString +="sr_no,Screen_Bri,CPUFreq,BT,CPUUtil,GPS State,num_of_sat,location_status \n";
//		logString +="sr_no,Screen_Bri,CPUFreq,CPUUtil,sent_packets,received_packest \n";     
//		logString += "sr_no,Screen_Bri,CPUFreq,CPUUtil\n";
		//int packetCount=0;

		logString +="sr_no,Screen_Bri,CPUFreq,CPUUtil,BT,sent_packets,received_packest, GPS STATE, GPS_LOCATION_STATUS \n";     
		
		String temp=""; int link_speed=0;
		for (long iter=0;!Thread.interrupted();iter++) {
			//packetCount=getBTPacketsSent();
			//temp= mGPS.getGPSState() + "," + mGPS.getNumOfSatilites() + "," + mGPS.getLocationStatus() + ",";
			startTime = SystemClock.elapsedRealtime();
			logString += (startTime - loopStartTime)/1000 + ","
					+ mLCD.getBrightness() + ","
					+ (int) mCPU.calculateCPUFreq(iter) + ","
					+ mCPU.getCPUUtilization()+ ","
					+ mBluetoothAdapter_State + "," 
				//    + packetCount+ ","
					+getTransmittedPackets()+ "," + getReceivedPackets()+ ","// + link_speed+ ","
				//	+ (preUptime==SystemClock.uptimeMillis())+ ","
				//	+ mWifiManager.getWifiState()+ ","
					+ mGPS.getGPSState() + ","  + mGPS.getLocationStatus() 
					+" \n";
			 //Log.i(TAG,temp);
//			if(iter%15==0)
//				link_speed = mWifiManager.getConnectionInfo().getLinkSpeed();
			// Log.i(TAG, getTransmittedPackets()+":"+getTransmittedBytes()+"|"+getSentDatainMB()+","+getReceivedPackets()+":"+getReceivedBytes()+"|"+getReceivedDatainMB()+" speed:"+link_speed);
			// Log.i(TAG, "size:"+getTransmittedPacketSize());
			 if (logString.length() >= 20480) {
				try {
					logOut.write(logString.getBytes());
					logOut.flush();
					logString = "";
				} catch (IOException e) {
					Log.e(TAG, e.getMessage());
					e.printStackTrace();
				}
			}
			endTime = SystemClock.elapsedRealtime();
			try {
				Thread.sleep((MAX_INTERVAL - (endTime - startTime)) > 0 ? MAX_INTERVAL
						- (endTime - startTime)
						: 0);
			} catch (IllegalArgumentException e) {
				Log.e(TAG,
						"SleepTime:" + (endTime - startTime) + e.getMessage());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "ComponentsState Thread Interrupted!!");
				Log.d(TAG, "closing outPut Streams");
				break;
			}
			
			
			
		}

		try {
			if (logString.length() >= 0) {
				logOut.write(logString.getBytes());
				logOut.flush();
			}
			logOut.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		mLCD.onExit();
		mGPS.onExit();
		//mWiFi.onExit();
		onExit();
		saveLogToSdcard();
		

	}

	private void onExit()
	{
		if(mWakeLock.isHeld())
			mWakeLock.release();
		mContext.unregisterReceiver(broadcastReceiver);
		
	}
	
	private void saveLogToSdcard()
	{
		new Thread() {
	          public void start() {
	            File writeFile = new File(
	                Environment.getExternalStorageDirectory(), "ComponentState" + 
	                    System.currentTimeMillis() + ".log");
	            try {
	              BufferedInputStream logIn = new BufferedInputStream(
	                  mContext.openFileInput("ComponentState.log"));
	              BufferedOutputStream logOut = new BufferedOutputStream(
	                  new FileOutputStream(writeFile));

	              byte[] buffer = new byte[20480];
	              for(int ln = logIn.read(buffer); ln != -1;
	                      ln = logIn.read(buffer)) {
	                logOut.write(buffer, 0, ln);
	              }
	              logIn.close();
	              logOut.close();
	             // Toast.makeText(mContext, "Wrote log to " + writeFile.getAbsolutePath(),Toast.LENGTH_SHORT).show();
	              Log.e(TAG, "Wrote log to " +
	                             writeFile.getAbsolutePath());
	              
	              return;
	            } catch(java.io.EOFException e) {
//	              Toast.makeText(mContext, "Wrote log to " +
//	                             writeFile.getAbsolutePath(),
//	                             Toast.LENGTH_SHORT).show();
	              Log.e(TAG, "Wrote log to " +
	                             writeFile.getAbsolutePath());
	              return;
	            } catch(IOException e) {
	            	Log.e(TAG, e.getMessage());
	            }
	           
	            
	            new Handler(Looper.getMainLooper()).post(new Runnable() {
	    			@Override
	    			public void run() {
	    				 Toast.makeText(mContext, "Failed to write log to sdcard",
		                           Toast.LENGTH_SHORT).show();
	    			}
	    		});
	            
	          }
	        }.start();
	}
}
