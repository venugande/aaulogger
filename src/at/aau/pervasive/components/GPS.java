package at.aau.pervasive.components;

import org.xml.sax.DTDHandler;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class GPS {

	private static GPS instance=null;
	public static LocationListener locationListener; 
	private LocationManager locationManager;
	private GpsStatus.Listener gpsListener;
	private String gps_state = "UNKNOWN",location_status="",temp1="",temp2="";
	private GpsStatus lastStatus;
    private static String TAG = GPS.class.getSimpleName();
	protected GPS(Context context) {

		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		gpsListener = new GpsStatus.Listener() {

			@Override
			public void onGpsStatusChanged(int event) {

				switch (event) {
				case GpsStatus.GPS_EVENT_STARTED:
					gps_state += "GPS_SESSION_STARTED";
					break;

				case GpsStatus.GPS_EVENT_FIRST_FIX:
					gps_state += "FIRST_FIX";
					break;

				case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
					gps_state += "SATELLITE_STATUS_UPDATED";
					break;

				case GpsStatus.GPS_EVENT_STOPPED:
					gps_state += "GPS_SESSION_ENDED";

				default:
					break;
				}

				synchronized (this) {
					lastStatus = locationManager.getGpsStatus(lastStatus);
				}
			}
		};

		locationManager.addGpsStatusListener(gpsListener);
		
		// Define a listener that responds to location updates
	     locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.
		     // makeUseOfNewLocation(location);
		    	location_status+="UPDATED";
		    	Log.i(TAG, "location changed:"+location.getLatitude()+","+location.getLongitude()+"source:"+location.getProvider());
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {
		    	location_status+="CHANGED to"+status;
		    } 

		    public void onProviderEnabled(String provider) {
		    	
		    	location_status+="PROVIDER_ENABLED";
		    	
		    }

		    public void onProviderDisabled(String provider) {
		    	location_status+="PROVIDER_DISABLED";
		    }
		  };
		//  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0, 0,locationListener);
		//locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000, 0,locationListener);
	
		
		 
		
		
	}

	public void requestForUpdates(long interval)
	{
		
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, interval, 0, locationListener);
	    Log.i(TAG, "requested for updates");
	}
	
	public void removeUpdates()
	{
		locationManager.removeUpdates(locationListener);
		 Log.i(TAG, "removed updates");
	}
	
	public static GPS getInstance(Context context) {
		if (instance == null)
			instance = new GPS(context);

		return instance;
	}
	
	public String getGPSState() {
		temp1=gps_state;
		gps_state="";
		return temp1;
	}

	public String getLocationStatus()
	{
		temp2=location_status;
		location_status="";
		return temp2;
	}
	
	public String getNumOfSatilites() {
		int satellite_count = 0; String temp=" snr";
		if (lastStatus != null) {
			for (GpsSatellite satelite : lastStatus.getSatellites()) {
				satellite_count++;
				temp+=":"+satelite.getSnr();
			}
		}
		return satellite_count+temp;
	}

	public void onExit() {
		if (gpsListener != null) {
			locationManager.removeGpsStatusListener(gpsListener);
		}
		removeUpdates();
	}
	


}
