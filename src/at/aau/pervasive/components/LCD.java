package at.aau.pervasive.components;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.util.Log;
import at.aau.pervasive.training.util.SystemInfo;

public class LCD {
	private final String TAG = "LCD";
	private static final String[] BACKLIGHT_BRIGHTNESS_FILES = {
			"/sys/devices/virtual/leds/lcd-backlight/brightness",
			"/sys/devices/platform/trout-backlight.0/leds/lcd-backlight/brightness", };

	private Context mContext;
	private BroadcastReceiver broadcastReceiver;
	private boolean screenOn;
	private String brightnessFile;

	public LCD(Context context) {
		mContext = context;
		screenOn = true;

		if (context == null) {
			return;
		}

		broadcastReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				synchronized (this) {
					if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
						screenOn = false;
					} else if (intent.getAction().equals(
							Intent.ACTION_SCREEN_ON)) {
						screenOn = true;
					}
				}
			};
		};

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
		intentFilter.addAction(Intent.ACTION_SCREEN_ON);
		context.registerReceiver(broadcastReceiver, intentFilter);

		for (int i = 0; i < BACKLIGHT_BRIGHTNESS_FILES.length; i++) {
			if (new File(BACKLIGHT_BRIGHTNESS_FILES[i]).exists()) {
				brightnessFile = BACKLIGHT_BRIGHTNESS_FILES[i];
			}
		}
	}

	public int getBrightness() {
		int brightness;
	 
		if(!isScreenOn())
			return -1;
		
		if (brightnessFile != null) {
			brightness = (int) SystemInfo.getInstance().readLongFromFile(
					brightnessFile);
		} else {
			try {
				brightness = Settings.System.getInt(
						mContext.getContentResolver(),
						Settings.System.SCREEN_BRIGHTNESS);
			} catch (Settings.SettingNotFoundException ex) {
				Log.w(TAG, "Could not retrieve brightness information");
				return -1;
			}
		}

		return brightness;
	}

	public boolean isScreenOn() {
		return screenOn;
	}
	
	public void onExit(){
		mContext.unregisterReceiver(broadcastReceiver);
		Log.d(TAG, "unregistered BR for SCREEN STATE");
	}
	
}
