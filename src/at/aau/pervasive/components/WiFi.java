package at.aau.pervasive.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;

public class WiFi {

	private final WifiManager wifimanager;
	private final Context mContext;
	private int wifi_state = -1;
	private BroadcastReceiver mBroadcastReceiver;

	public WiFi(Context context) {
		mContext = context;
		wifimanager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);

		mBroadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(
						WifiManager.WIFI_STATE_CHANGED_ACTION)) {

					wifi_state = intent.getIntExtra(
							WifiManager.EXTRA_WIFI_STATE, -1);

				}
			}
		};
		
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		mContext.registerReceiver(mBroadcastReceiver, intentFilter);

	}
	
	public int getWifiState()
	{
		return wifi_state;
	}
	
	public void onExit()
	{
		mContext.unregisterReceiver(mBroadcastReceiver);
	}
	

}
