package at.aau.pervasive.components;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.util.Log;
import android.util.SparseArray;
import at.aau.pervasive.training.PowerData;
import at.aau.pervasive.training.util.Recycler;
import at.aau.pervasive.training.util.SystemInfo;
 
public class CPU  {
  public static class CpuData extends PowerData {
    private static Recycler<CpuData> recycler = new Recycler<CpuData>();

    public static CpuData obtain() {
      CpuData result = recycler.obtain();
      if(result != null) return result;
      return new CpuData();
    }

    @Override
    public void recycle() {
      recycler.recycle(this);
    }

    public double sysPerc;
    public double usrPerc;
    public double freq;

    private CpuData() {
    }

    public void init(double sysPerc, double usrPerc, double freq) {
      this.sysPerc = sysPerc;
      this.usrPerc = usrPerc;
      this.freq = freq;
    }

    public void writeLogDataInfo(OutputStreamWriter out) throws IOException {
      StringBuilder res = new StringBuilder();
      res.append("CPU-sys ").append((long)Math.round(sysPerc))
         .append("\nCPU-usr ").append((long)Math.round(usrPerc))
         .append("\nCPU-freq ").append(freq)
         .append("\n");
     
      out.write(res.toString());
    }
  }

  private static final String TAG = "CPU";
  private static final String CPU_FREQ_FILE = "/proc/cpuinfo";
  private static final String STAT_FILE = "/proc/stat";

  private CpuStateKeeper cpuState;
  private SparseArray<CpuStateKeeper> pidStates;
  private SparseArray<CpuStateKeeper> uidLinks;

  private int[] pids;
  private long[] statsBuf;


  public CPU() {
    cpuState = new CpuStateKeeper(SystemInfo.AID_ALL);
    pidStates = new SparseArray<CpuStateKeeper>();
    uidLinks = new SparseArray<CpuStateKeeper>();
    statsBuf = new long[7];
  }

  public double calculateCPUFreq(long iteration) {
   // IterationData result = IterationData.obtain();

    SystemInfo sysInfo = SystemInfo.getInstance();
    double freq = readCpuFreq(sysInfo);
    if(freq < 0) {
      Log.w(TAG, "Failed to read cpu frequency");
      return -1;
    }

    if(!sysInfo.getUsrSysTotalTime(statsBuf)) {
      Log.w(TAG, "Failed to read cpu times");
      return -2;
    }

    long usrTime = statsBuf[SystemInfo.INDEX_USER_TIME];
    long sysTime = statsBuf[SystemInfo.INDEX_SYS_TIME];
    long totalTime = statsBuf[SystemInfo.INDEX_TOTAL_TIME];

    boolean init = cpuState.isInitialized();
    cpuState.updateState(usrTime, sysTime, totalTime, iteration);
   
    if(init) {
      CpuData data = CpuData.obtain();
      data.init(cpuState.getUsrPerc(), cpuState.getSysPerc(), freq);
      //result.setPowerData(data);
     // Log.i(TAG, (cpuState.getUsrPerc() + cpuState.getSysPerc()) +", "+ freq);
    }


    return freq;
  }

  public double getCPUUtilization()
  {
	  return cpuState.getSysPerc() + cpuState.getUsrPerc();
  }

  private static class CpuStateKeeper {
    private int uid;
    private long iteration;
    private long lastUpdateIteration;
    private long inactiveIterations;

    private long lastUsr;
    private long lastSys;
    private long lastTotal;

    private long sumUsr;
    private long sumSys;
    private long deltaTotal;

    private CpuStateKeeper(int uid) {
      this.uid = uid;
      lastUsr = lastSys = -1;
      lastUpdateIteration = iteration = -1;
      inactiveIterations = 0;
    }

    public boolean isInitialized() {
      return lastUsr != -1;
    }

    public void updateIteration(long iteration, long totalTime) {
      /* Process is still running but actually reading the cpu utilization has
       * been skipped this iteration to avoid wasting cpu cycles as this process
       * has not been very active recently. */
      sumUsr = 0;
      sumSys = 0;
      deltaTotal = totalTime - lastTotal;
      if(deltaTotal < 1) deltaTotal = 1;
      lastTotal = totalTime;
      this.iteration = iteration;
    }

    public void updateState(long usrTime, long sysTime, long totalTime,
                            long iteration) {
      sumUsr = usrTime - lastUsr;
      sumSys = sysTime - lastSys;
      deltaTotal = totalTime - lastTotal;
      if(deltaTotal < 1) deltaTotal = 1;
      lastUsr = usrTime;
      lastSys = sysTime;
      lastTotal = totalTime;
      lastUpdateIteration = this.iteration = iteration;

      if(getUsrPerc() + getSysPerc() < 0.1) {
        inactiveIterations++;
      } else {
        inactiveIterations = 0;
      }
    }

    public int getUid() {
      return uid;
    }

    public void absorb(CpuStateKeeper s) {
      sumUsr += s.sumUsr;
      sumSys += s.sumSys;
    }

    public double getUsrPerc() {
      return 100.0 * sumUsr / Math.max(sumUsr + sumSys, deltaTotal);
    }

    public double getSysPerc() {
      return 100.0 * sumSys / Math.max(sumUsr + sumSys, deltaTotal);
    }

    public boolean isAlive(long iteration) {
      return this.iteration == iteration;
    }

    public boolean isStale(long iteration) {
      return 1L << (iteration - lastUpdateIteration) > 
              inactiveIterations * inactiveIterations;
    }
  }

  public boolean hasUidInformation() {
    return true;
  }

  public String getComponentName() {
    return "CPU";
  }

  /* Returns the frequency of the processor in Mhz.  If the frequency cannot
   * be determined returns a negative value instead.
   */
  private double readCpuFreq(SystemInfo sysInfo) {
    /* Try to read from the /sys/devices file first.  If that doesn't work
     * try manually inspecting the /proc/cpuinfo file.
     */
    long cpuFreqKhz = sysInfo.readLongFromFile(
      "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
    if(cpuFreqKhz != -1) {
      return cpuFreqKhz / 1000.0;
    }

    FileReader fstream;
    try {
      fstream = new FileReader(CPU_FREQ_FILE);
    } catch (FileNotFoundException e) {
      Log.w(TAG, "Could not read cpu frequency file");
      return -1;
    }
    BufferedReader in = new BufferedReader(fstream, 500);
    String line;
    try {
      while((line = in.readLine()) != null) {
        if(line.startsWith("BogoMIPS")) {
          return Double.parseDouble(line.trim().split("[ :]+")[1]);
        }
      }
    } catch(IOException e) {
      /* Failed to read from the cpu freq file. */
    } catch(NumberFormatException e) {
      /* Frequency not formatted properly as a double. */
    }
    Log.w(TAG, "Failed to read cpu frequency");
    return -1;
  }
}
