package at.aau.pervasive.components;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.bluetooth.BluetoothAdapter;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;
import at.aau.pervasive.training.AAULoggerService;

public class Bluetooth extends Thread {

	private final static String TAG=Bluetooth.class.getSimpleName();
	private BluetoothAdapter mBluetoothAdapter;
	private static int WAITING_INTERVAL=1000;
	private static int SLEEP_INTERVAL=5000;
	private final AAULoggerService mLoggerService;
	private final int BLUETOOTH_ENABLED = 1;
	private final int BLUETOOTH_DISCOVERABLE =2;
	private final File mOutFile;
	private BufferedOutputStream logOut;

	public Bluetooth(AAULoggerService loggerService) {
		mLoggerService=loggerService;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mOutFile = new File(Environment.getExternalStorageDirectory(),
				"BluetoothState" + System.currentTimeMillis() + ".log");
		try {
			logOut = new BufferedOutputStream(new FileOutputStream(mOutFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	

	}

	public void run() {
		android.os.Process
				.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
		
		//bluetooth enable/disable exercise 
		try {
			logOut.write(new String("From	To	State\n").getBytes());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int counter=0;counter<5&&!Thread.interrupted();) {
			
			long curTime = SystemClock.elapsedRealtime();
			mBluetoothAdapter.enable();
			try {
				sleep(WAITING_INTERVAL);
			} catch (InterruptedException e) {
			
				break;
			}
			mBluetoothAdapter.disable();
			long stopTime = SystemClock.elapsedRealtime();
			try {
			Log.e(TAG, "From:"+curTime+" To:"+stopTime+ "Bluetooth Enabeled");
			logOut.write(new String( curTime+","+stopTime+ ","+BLUETOOTH_ENABLED+"\n").getBytes());
			logOut.flush();
			
				sleep(SLEEP_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			counter++;
		}
		
		if(!mBluetoothAdapter.isEnabled())
		mBluetoothAdapter.enable();
		
		//remote device discovery exercise	
		for(int counter=0;counter<5&&!Thread.interrupted();)
		{
			long curTime = SystemClock.elapsedRealtime();
			mBluetoothAdapter.startDiscovery();
			try {
				Thread.sleep(WAITING_INTERVAL);
			} catch (InterruptedException e) {
		      break;
			}
			mBluetoothAdapter.cancelDiscovery();
			long stopTime = SystemClock.elapsedRealtime();
			WAITING_INTERVAL+=5000;
			Log.d(TAG, "From:"+curTime+" To:"+stopTime+ " device discovery enabled");
			try {
			//logOut.write(new String("From:"+curTime+" To:"+stopTime+ " device discovery enabled \n").getBytes());
				logOut.write(new String( curTime+","+stopTime+ ","+BLUETOOTH_DISCOVERABLE+" \n").getBytes());
				logOut.flush();
				Thread.sleep(SLEEP_INTERVAL);
			} catch (InterruptedException e) {
				//e.printStackTrace();
				Log.e(TAG, "Bluetooth Thread Interrupted!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			counter++;
		}
		WAITING_INTERVAL=5000;
		Log.d(TAG, "Bluetooth Thread finished running");
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(mLoggerService, "finished running training prog", Toast.LENGTH_LONG).show();
			}
		});
		
		mBluetoothAdapter.disable(); 
		mLoggerService.stopSelf();
		
		try {
			Log.e(TAG, "closing outPut Streams");
			logOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
