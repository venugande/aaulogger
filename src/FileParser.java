import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class FileParser {
public static void main(String[] args) {
	try {
		String filePath= getFilePathPrefix();
		long[] bluetooth_state_timepairs = new long[20]; 
		int[] bluetooth_state_values = new int[10];
		BufferedReader br = new BufferedReader(new FileReader(filePath+"bluetooth_state.log"));
		br.readLine();
		String str;
		int index=0;
		while((str= br.readLine())!=null)
		{
			//System.out.println(str);
			String temp[] = str.split(",");
			bluetooth_state_values[(index+1)/2] = Integer.parseInt(temp[2].trim());
			bluetooth_state_timepairs[index] = Long.parseLong(temp[0]);
			index++;
			bluetooth_state_timepairs[index] = Long.parseLong(temp[1]);
			index++;
			
		}
		
		br.close();
		
		BufferedReader inFile = new BufferedReader(new FileReader(filePath+"component_state.log"));
		BufferedWriter outFile = new BufferedWriter(new FileWriter(filePath+"state.log"));
		str=inFile.readLine();
		//long startTime = Long.parseLong(str.split(":")[1]);
		outFile.write(str+"\n");
		String outStr="";
		int tempIndex=0;
		long temp_time;
		int bState;
		while ((str=inFile.readLine())!=null) {
			String temp[]=str.split(",");
			temp_time =Long.parseLong(temp[0]);//+startTime;
			if(temp_time > bluetooth_state_timepairs[tempIndex+1] && tempIndex< bluetooth_state_timepairs.length-2)
				tempIndex+=2;
			
			if(temp_time>= bluetooth_state_timepairs[tempIndex] && temp_time<=bluetooth_state_timepairs[tempIndex+1])
				bState=bluetooth_state_values[(tempIndex+1)/2];
			else
				bState=0;
			
			outStr=outStr+str+","+bState+"\n";
			if(outStr.length()>2048)
			{
				outFile.write(outStr);
				outFile.flush();
			    outStr="";
			}
		}
		
		outFile.write(outStr);
		outFile.flush();
		outFile.close();
		
		inFile.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}

   static String getFilePathPrefix() {
		String OS = System.getProperty("os.name").toLowerCase();

		if (OS.indexOf("win") >= 0)
			return "C:\\Users\\PC-Lab\\Desktop\\";

		return "/Users/venu/Desktop/";
	}
}
