import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Correction {

	public static void main(String[] args) {
		String filePath= FileParser.getFilePathPrefix();
		try {
			BufferedReader inFile = new BufferedReader(new FileReader(filePath+"state.log"));
			BufferedWriter outFile = new BufferedWriter(new FileWriter(filePath+"state1.log"));
		    int index =0, count =0, tmp_index=0;
		    String str=inFile.readLine();
		    outFile.write(str+"\n");
		    final int frequency=1;
		    while ((str=inFile.readLine())!=null) {
				tmp_index= Integer.parseInt(str.split(",")[0]);
				if(index==tmp_index)
				{
				outFile.write(str+"\n");
				count++;
				 if(count==frequency)
				 {
					 index++;
					 count=0;
				 }
					 
				}else
				{
					for(;count<frequency;)
					{
						outFile.write(index+",-1,1600,0.0,0,0,0"+"\n");
						count++;
						 if(count==frequency)
						 {
							 index++;
							 count=0;
						 }
						if (index==tmp_index)
							break;
					}
					
					outFile.write(str+"\n");
					count++;
					 if(count==frequency)
					 {
						 index++;
						 count=0;
					 }
				}
				
				
			}
		    outFile.flush();
			outFile.close();
			inFile.close();
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
