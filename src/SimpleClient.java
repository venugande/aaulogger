import java.net.*;
import java.io.*;
public class SimpleClient {
 public static void main(String args[]) throws IOException {
 // Open your connection to a server, at port 1254
long start_time = System.currentTimeMillis();	 
 Socket s1 = new Socket("143.205.199.252",4444);
 // Get an input file handle from the socket and read the input
 InputStream s1In = s1.getInputStream();
 DataInputStream dis = new DataInputStream(s1In);
 String st = new String (dis.readUTF());
 long end_time = System.currentTimeMillis();
 long server_time =Long.parseLong(st)-((end_time-start_time)/2);
 System.out.println("response:"+st+" RTT:"+(end_time-start_time)+" ServerTime:"+server_time +"Local Time:"+end_time+ " diff:"+(end_time-server_time));
 // When done, just close the connection and exit
 dis.close();
 s1In.close();
 s1.close();
 }
}