import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AggregateData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		long powermonitor_statr_time = Long.parseLong(args[0]);
		long traingprog_start_time = Long.parseLong(args[1]);

		String[] temp;

		try {
			BufferedReader power_file = new BufferedReader(new FileReader(
					FileParser.getFilePathPrefix() + "powerbox_averaged.log"));

			BufferedReader component_state_file = new BufferedReader(
					new FileReader(FileParser.getFilePathPrefix() + "state1.log"));

			BufferedWriter outFile = new BufferedWriter(new FileWriter(
					FileParser.getFilePathPrefix() + "aggregated_data.log"));

			long skip = (long) ((traingprog_start_time - powermonitor_statr_time) * 0.02);

			System.out.println("Time diff:"+(traingprog_start_time - powermonitor_statr_time)+" skip:"+skip);
			
			String str1 = "", str2 = "", tempOut = "";

			for (long i = 0; i < skip
					&& ((str1 = power_file.readLine()) != null); i++)
				;
			//component_state_file.readLine();
			//power_file.readLine();
			while (true) {
				str1 = power_file.readLine();

				str2 = component_state_file.readLine();

				if (str1 == null || str2 == null)
					break;
				System.out.println(str1);
				// to round the power value
				temp = str1.split(",");
			//	str1 = temp[0] + ","						+ (int) Math.round(Double.parseDouble(temp[1]));

				tempOut = tempOut + str1 + "," + str2 + "\n";
				if (tempOut.length() > 2048) {
					outFile.write(tempOut);
					outFile.flush();
					tempOut = "";
				}

			}
			outFile.write(tempOut);
			outFile.flush();
			outFile.close();

			power_file.close();
			component_state_file.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
